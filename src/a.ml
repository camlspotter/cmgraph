let (&) = (@@)

let args = Array.to_list Sys.argv

module Parsec = struct
  type 'a t = string list -> ('a * string list, string) result
  
  let return : 'a -> 'a t = fun a -> fun xs -> Ok (a, xs)
  
  let fail : string -> 'a t = fun s _ -> Error s
  
  let eos = function
    | [] -> Ok ((), [])
    | x::_ -> Error ("eos: remains: " ^ x)
  
  let (>>=) at f = fun xs -> match at xs with
    | Error e -> Error e
    | Ok (v, xs) -> f v xs
  
  let (>>) at f = fun xs -> match at xs with
    | Error e -> Error e
    | Ok ((), xs) -> f xs
  
  let (>>|) at f = fun xs -> match at xs with
    | Error e -> Error e
    | Ok (v, xs) -> Ok (f v, xs)
  
  let any = function
    | x :: xs -> Ok (x, xs)
    | [] -> Error "eos"
  
  let result f ok error = fun xs -> match f xs with
    | Ok (v, xs) -> ok v xs
    | Error e -> error e xs
  
  let rec star (f : 'a t) : 'a list t =
    result f 
      (fun v -> star f >>| fun vs -> v::vs)
      (fun _ -> return [])
  
  let plus (f : 'a t) : 'a list t = 
    f >>= fun v -> star f >>| fun vs -> v::vs
  
  let string s = function 
    | x :: xs when x = s -> Ok ((), xs)
    | _ :: _ -> Error "unexpected"
    | [] -> Error "eos"
  
  let non_switch =
    any >>= fun s ->
    if try String.get s 0 = '-' with _ -> false then fail "it is a switch"
    else return s
end

module P = Parsec

module A = struct
  type 'a t = 'a P.t * string
    
  let switch_no_arg name doc = 
    let open P in
    let n = "--" ^ name in
    string n,
    n ^ " : " ^ doc
  
  let switch_with_arg name g (f, docf) doc = 
    let open P in
    let n = "--" ^ name in
    string n >> f >>| g, 
    n ^ " " ^ docf ^ " : " ^ doc
  
  let non_empty_list (f, docf) =
    let open P in
    plus f, Printf.sprintf "%s ..." docf
    
  let maybe_empty_list (f, docf) =
    let open P in
    star f, Printf.sprintf "[%s ...]" docf
    
  let non_switch name = 
    let open P in
    non_switch, name
    
  let one_of doc xs =
    let fs = List.map fst xs in
    let docs = List.map snd xs in
    let doc = doc ^ ": " ^ String.concat ", " docs in 
    let open P in
    let rec g = function
      | [] -> fail "switches: not possible"
      | f::fs -> result f return (fun _ -> g fs)
    in
    g fs, doc

  let list_of_one_of doc xs =
    let fs = List.map fst xs in
    let docs = List.map snd xs in
    let doc = doc ^ ": " ^ String.concat ", " docs in 
    let open P in
    let rec g = function
      | [] -> fail "not match"
      | h::hs -> result h return (fun _e -> g hs)
    in
    star (g fs), doc

  let package_build_dirs = 
    switch_with_arg "package-build-dirs" (fun x -> `Package x) (non_empty_list (non_switch "dir"))
      "hahaha"
  
  let only_linked_from = 
    switch_with_arg "only-linked-from" (fun x -> `Only_linked_from x) (non_empty_list (non_switch "module"))
      "hahaha"

  let only_dependent_on = 
    switch_with_arg "only-dependent-on" (fun x -> `Only_dependent_on x) (non_empty_list (non_switch "module"))
      "hahaha"

  let switches = list_of_one_of "options" 
      [ package_build_dirs
      ; only_linked_from
      ; only_dependent_on
      ]
(*
  let package_build_dirs = string "-package-build-dirs" >> plus non_switch >>| fun x -> `PackBuildDirs x
  let only_linked_from   = string "-only-linked-from"   >> non_switch >>| fun x -> `OnlyLinkedFrom x
  let only_dependent_on  = string "-only-dependent-on"  >> non_switch >>| fun x -> `OnlyDependentOn x
  let nop = string "-nop"
  
  let rec or_ = function
    | [] -> fail "or_: not possible"
    | f::fs -> result f return (fun _ -> or_ fs)
*)
end

(*
let test () =
  match 
    (star (or_ [ package_build_dirs; only_linked_from; only_dependent_on ])
       >>= fun res -> eos >> return res)
      [ "-package-build-dirs"; "a"; "b"; 
        "-package-build-dirs"; "a"; "b"; ]

  with
  | Error e -> failwith e
  | Ok ([`PackBuildDirs ["a"; "b"]; `PackBuildDirs ["a"; "b"]], _) -> ()
  | Ok _ -> assert false
*)
  
